package gomuxer

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"
)

type Muxer struct {
	Methods map[string]Method
}

type Method func(*Request) *Response

type Request struct {
	Action    string                 `json:"action"`
	Arguments map[string]interface{} `json:"arguments,omitempty"`
}

type Response struct {
	Success bool                   `json:"success"`
	Data    map[string]interface{} `json:"data,omitempty"`
	Message string                 `json:"message,omitempty"`
}

var (
	ErrWrongHttpMethod    = errors.New("Wrong HTTP Method. There is must to be POST.")
	ErrWrongContentType   = errors.New("Wrong Content-Type header. There is must to be \"application/json\".")
	ErrWrongContentLength = errors.New("Content-Length <= 0, you must to provide the right request.")
	ErrCantReadBody       = errors.New("Something went wrong. Cannot read http body.")
	ErrCantUnmarshalJson  = errors.New("Something went wrong. Wrong JSON or wrong struct of request, JSON-array was expected.")
)

func NewDemuxer() *Muxer {
	muxer := Muxer{
		Methods: make(map[string]Method),
	}

	return &muxer
}

func (m *Muxer) RequestHander(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.Error(w, ErrWrongHttpMethod.Error(), 400)
		return
	}

	if r.ContentLength <= 0 {
		http.Error(w, ErrWrongContentLength.Error(), 400)
		return
	}

	foundJson := false
	for _, ctValue := range r.Header["Content-Type"] {
		if strings.Contains(ctValue, "application/json") {
			foundJson = true
			break
		}
	}

	if foundJson == false {
		http.Error(w, ErrWrongContentType.Error(), 400)
		return
	}

	jsonBytes, err := ioutil.ReadAll(r.Body)

	if err != nil {
		http.Error(w, ErrCantReadBody.Error(), 400)
		return
	}

	var muxRequests []Request

	fmt.Printf("%s\n", string(jsonBytes))

	err = json.Unmarshal(jsonBytes, &muxRequests)

	if err != nil {
		http.Error(w, ErrCantUnmarshalJson.Error(), 400)
		return
	}

	var muxResponseChans []chan Response = make([]chan Response, 0, len(muxRequests))
	var muxResponses []Response

	for i := range muxRequests {
		muxResponseChans = append(muxResponseChans, timeout(m.handleMuxRequest(&muxRequests[i]), time.Second*9))
	}

	muxResponses = resolveSequentially(muxResponseChans)

	fmt.Printf("%#v", muxResponses)

	jsonBytes, err = json.Marshal(muxResponses)

	if err != nil {
		fmt.Println("M error")
		return
	}

	w.Write(jsonBytes)
}

func (m *Muxer) AddMethod(name string, method Method) {
	m.Methods[name] = method
}

func (m *Muxer) handleMuxRequest(req *Request) chan Response {
	c := make(chan Response)
	go func() {
		if methodHandler, ok := m.Methods[req.Action]; !ok {
			c <- Response{
				Success: false,
				Message: "Method not found",
			}
		} else {
			//fmt.Println("Handling ...", *req)
			c <- *methodHandler(req)
		}
	}()
	return c
}

func timeout(res chan Response, d time.Duration) chan Response {
	c := make(chan Response)

	go func() {
		select {
		case <-time.After(d):
			c <- Response{
				Success: false,
				Message: "Timeout request",
			}
		case t := <-res:
			//fmt.Println(t)
			c <- t
		}
	}()

	return c
}

func resolveSequentially(chans []chan Response) []Response {
	res := make([]Response, 0, len(chans))

	for _, ch := range chans {
		res = append(res, <-ch)
	}

	return res
}
